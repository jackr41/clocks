package clock;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;


/**
 * A simple clock applet that displays different faces for the clock.
 * A button allows the user to see different views of the clock.
 * 
 * @author cusack, October 2001. Extensive refactoring, January 2013.
 */
public class ClockGUI extends JFrame {
	private ClockInterface				clock;

	private ArrayList<ClockView>	clockView;
	private int						placedClock;
	private JButton					togButton;

	
	public static void main(String[] args) {
		new ClockGUI();
	}
	/**
	 * Set up the application by putting the clocks and buttons on, etc.
	 */
	public  ClockGUI() {
		clock = new ClockModel();
		Timer tickTimer = new Timer(100, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				updateTime();
			}
		});
		tickTimer.start();

		clockView = new ArrayList<ClockView>();
		clockView.add(new DigitalClock(clock));
		clockView.add(new AnalogClock(clock));
		clockView.add(new BarClock(clock));

		togButton = new JButton("Toggle View");
		togButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nextClock();
			}
		});

		Container c = this.getContentPane();
		c.setLayout(new BorderLayout());
		c.add(togButton, BorderLayout.SOUTH);

		placedClock = clockView.size() - 1;
		nextClock();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		pack();
		setSize(500, 550);
		setVisible(true);
	}

	/**
	 * Switch the clock view to the next one on the list.
	 */
	public void nextClock() {
		Container c = this.getContentPane();
		c.remove(clockView.get(placedClock));
		placedClock = (placedClock + 1) % clockView.size();
		c.add(clockView.get(placedClock), BorderLayout.CENTER);
		c.validate();
		repaint();
	}


	/**
	 * Set the time of the clock to the current time and update the display.
	 */
	private void updateTime() {
		Calendar now = Calendar.getInstance();
		clock.setSeconds(now.get(Calendar.SECOND));
		clock.setMinutes(now.get(Calendar.MINUTE));
		clock.setHours(now.get(Calendar.HOUR));
		this.repaint();
	}
}
